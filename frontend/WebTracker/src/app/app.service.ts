import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private baseUrl = `${environment.apiUrl}`
  constructor(private httpClient: HttpClient) { }


  public getDelivery(id: string) {
    return this.httpClient.get(`${this.baseUrl}/delivery/${id}`);
  }

  public getPackage(id: string) {
    return this.httpClient.get(`${this.baseUrl}/package/${id}`);
  }


}
