import { AppService } from './app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { webSocket, WebSocketSubject } from "rxjs/webSocket";
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd/message';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  title = 'WebTracker';
  showDetails = false

  public deliveryPosition: any = {
    label: {
      text: 'Delivery',
    },
    title: 'Current delivery position',
    options: { animation: google.maps.Animation.BOUNCE },
  };
  public source: any = {};
  public destination: any = {};
  public packageDetails: any = {}
  public deliveryDetails: any = {}

  subject!: WebSocketSubject<any>;

  zoom = 9;
  center!: google.maps.LatLngLiteral
  options: google.maps.MapOptions = {
    zoomControl: true,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 8,
    // clickableIcons: true,
  }
  trackingForm!: FormGroup;


  constructor(private fb: FormBuilder, private appService: AppService, private messageService: NzMessageService, private cdr: ChangeDetectorRef) { }


  ngOnInit(): void {
    this.getLocation();
    this.trackingForm = this.fb.group({
      package_id: [null, [Validators.required]],
    });
  }



  /**
   * get delivery and package details
   *
   * @memberof AppComponent
   */
  submitForm(): void {


    if (this.trackingForm.valid) {


      this.appService.getPackage(this.trackingForm.value.package_id)
        .subscribe(
          pkg => {
            this.packageDetails = pkg;
            this.setPackageLocations(this.packageDetails);
            this.showDetails = true;

            if (this.packageDetails.active_delivery_id) {
              this.appService.getDelivery(this.packageDetails.active_delivery_id)
                .subscribe(delivery => {
                  this.deliveryDetails = delivery;
                  this.cdr.detectChanges();

                  this.subject = webSocket(`${environment.wsUrl}/delivery/${this.packageDetails.active_delivery_id}`);
                  this.subject.subscribe(message => {
                    // console.log(message)
                    if (message.event == 'delivery_updated') {
                      this.deliveryDetails = { ...message.delivery_object }
                      this.messageService.info('delivery updated')
                      this.updateDeliveryLocation(message.delivery_object.location)
                      this.cdr.detectChanges()
                    }
                  })
                })
            }
            this.cdr.detectChanges();
          }
        )

    } else {
      Object.values(this.trackingForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }


  }
  /**
   * Update delivery location
   *
   * @param {*} location
   * @memberof AppComponent
   */
  updateDeliveryLocation(location: any) {
    this.deliveryPosition.position = { lat: location.lat, lng: location.long };
  }

  /**
   * Set source and Destiantion positions
   *
   * @param {*} pkg
   * @memberof AppComponent
   */
  setPackageLocations(pkg: any) {

    this.source = {
      position: {
        lat: pkg.from_location.lat,
        lng: pkg.from_location.long,
      },
      label: {
        text: 'Source',
      },
      title: 'Package source postion',
      options: { animation: google.maps.Animation.DROP },
    }
    this.destination = {
      position: {
        lat: pkg.to_location.lat,
        lng: pkg.to_location.long,
      },
      label: {
        text: 'Destination',
      },
      title: 'Package destination postion',
      options: { animation: google.maps.Animation.DROP },
    }

  }

  /**
   * get current user location
   */
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: GeolocationPosition) => {
        if (position) {

          this.center = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          }
        }
      },
        (error: GeolocationPositionError) =>
          this.messageService.error(error.message)
      );
    } else {
      this.messageService.error("Geolocation is not supported by this browser.");
    }
  }
}
