import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable, throwError } from 'rxjs';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';



/**
 * Http error interceptor
 */
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private notificationService: NzNotificationService, private router: Router) {

  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMsg = '';


          // if (error.status == 401) {
          //   this.router.navigateByUrl("/auth")
          // }

          errorMsg = `<p>An error occured. Please try again later</p>`;

          if (error.status == 404) {
            errorMsg = `<p> The requested resource was <b>not found</b> on the server</p>`
          }

          if (error.status == 400) {
            errorMsg = `<p><b> Bad request</b> please verify your request and try again</p>`
          }

          this.notificationService.error('Request failed!', `${errorMsg}`)
          return throwError(errorMsg);
        })
      )
  }
}
