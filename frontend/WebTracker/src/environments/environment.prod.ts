export const environment = {
  production: true,
  apiUrl: "/api",
  wsUrl: "ws://localhost:9090/websocket"
};
