import { AppService } from './app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { webSocket, WebSocketSubject } from "rxjs/webSocket";
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd/message';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  title = 'WebDriver';
  showDetails = false

  public currentPosition: any = {};
  public source: any = {};
  public destination: any = {};
  public packageDetails: any = {}
  public deliveryDetails: any = {}

  subject!: WebSocketSubject<any>;

  zoom = 9;
  center!: google.maps.LatLngLiteral
  options: google.maps.MapOptions = {
    zoomControl: true,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 8,
    clickableIcons: true,
  }
  trackingForm!: FormGroup;


  constructor(private fb: FormBuilder, private appService: AppService, private messageService: NzMessageService, private cdr: ChangeDetectorRef) { }


  ngOnInit(): void {
    this.getLocation();
    this.trackingForm = this.fb.group({
      delivery_id: [null, [Validators.required]],
    });
  }



  /**
   * get delivery and package details
   *
   * @memberof AppComponent
   */
  submitForm(): void {


    if (this.trackingForm.valid) {
      this.appService.getDelivery(this.trackingForm.value.delivery_id)
        .subscribe(delivery => {
          this.deliveryDetails = delivery;

          if (this.deliveryDetails.package_id) {
            this.appService.getPackage(this.deliveryDetails.package_id)
              .subscribe(
                pkg => {
                  this.packageDetails = pkg;
                  this.setPackageLocations(this.packageDetails)
                  this.showDetails = true
                  this.subject = webSocket(`${environment.wsUrl}/delivery/${this.deliveryDetails.delivery_id}`);
                  this.subject.subscribe(message => {
                    // console.log(message)
                    if (message.event == 'delivery_updated') {
                      this.deliveryDetails = {...message.delivery_object}
                      this.messageService.info('delivery updated')
                      this.cdr.detectChanges()
                    }
                  })

                  setInterval(() => {
                    this.sendLocationChanged(this.deliveryDetails.delivery_id, this.subject)
                  }, 20000)

                }
              )
          }

        })
    } else {
      Object.values(this.trackingForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }


  }


  /**
   *
   * @param delivery_id Send location update event and update current position
   * @param wsSubject
   */
  sendLocationChanged(delivery_id: string, wsSubject: WebSocketSubject<any>) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: GeolocationPosition) => {
        if (position) {
          // if (this.currentPosition.position.lat != position.coords.latitude && this.currentPosition.position.lng != position.coords.longitude) {
          this.currentPosition.position.lat = position.coords.latitude;
          this.currentPosition.position.lng = position.coords.longitude;
          this.center = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }

          const payload = {
            event: 'location_changed',
            delivery_id: delivery_id,
            location: {
              lat: position.coords.latitude,
              long: position.coords.longitude
            }
          }

          wsSubject.next(payload);
          // }
        }
      },
        (error: GeolocationPositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  /**
   * send status change event
   *
   * @param {string} status
   * @memberof AppComponent
   */
  sendStatusChanged(status: string) {

  const payload = { event: 'status_changed', delivery_id: this.deliveryDetails.delivery_id, status: status }
    // console.log(status, this.deliveryDetails, this.subject);
    this.subject.next(payload);
  }

  /**
   * Set source and Destiantion positions
   *
   * @param {*} pkg
   * @memberof AppComponent
   */
  setPackageLocations(pkg: any) {

    this.source = {
      position: {
        lat: pkg.from_location.lat,
        lng: pkg.from_location.long,
      },
      label: {
        text: 'Source',
      },
      title: 'Package source postion',
      options: { animation: google.maps.Animation.DROP },
    }
    this.destination = {
      position: {
        lat: pkg.to_location.lat,
        lng: pkg.to_location.long,
      },
      label: {
        text: 'Destination',
      },
      title: 'Package destination postion',
      options: { animation: google.maps.Animation.DROP },
    }

  }

  /**
   * get current user location
   */
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: GeolocationPosition) => {
        if (position) {

          this.currentPosition = {
            position: {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            },
            label: {
              text: 'You',
            },
            title: 'Your current position',
            options: { animation: google.maps.Animation.BOUNCE },
          }

          this.center = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          }
        }
      },
        (error: GeolocationPositionError) =>
          this.messageService.error(error.message)
      );
    } else {
      this.messageService.error("Geolocation is not supported by this browser.");
    }
  }
}
