import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  baseURL: string = `${environment.apiUrl}`;

  constructor(private httpClient: HttpClient) { }

  public getAllPackages() {
    return this.httpClient.get(`${this.baseURL}/package/`);
  }


  public getAllDeliveries() {
    return this.httpClient.get(`${this.baseURL}/delivery/`);
  }

  public cretePackage(body: any) {
    return this.httpClient.post(`${this.baseURL}/package/`, body);
  }

  public creteDelivery(body: any) {
    return this.httpClient.post(`${this.baseURL}/delivery/`, body);
  }
}
