import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-create-delivery',
  templateUrl: './create-delivery.component.html',
  styleUrls: ['./create-delivery.component.scss']
})
export class CreateDeliveryComponent implements OnInit {

  public deliveryForm!: FormGroup;
  public packages: any[] = [];
  public filteredOptions: any[] = []
  constructor(private fb: FormBuilder, private appService: AppService, private notificationService: NzNotificationService) { }

  ngOnInit(): void {
    this.deliveryForm = this.fb.group({
      package_id: [null, [Validators.required]],
      pickup_time: [null, []],
      start_time: [null, []],
      end_time: [null, []],
      status: ['open', []],
      location_lat: [null, []],
      location_long: [null, []],
    })
    this.appService.getAllPackages().subscribe(
      (data: any) => {
        this.packages = data;
        this.filteredOptions = data;
      }
    )

  }

  /**
   * filter package for auto complet
   * @param value
   */
  onPackageChange(value: string): void {
    if (value) {
      this.filteredOptions = this.packages.filter(option => (option.description && option.description.toLowerCase().indexOf(value.toLowerCase()) !== -1
        ||
        option.package_id && option.package_id.toLowerCase().indexOf(value.toLowerCase()) !== -1));
    }

  }

  submitForm() {
    if (this.deliveryForm.valid) {
      console.log(this.deliveryForm.value)

      let body = this.deliveryForm.value;

      if (body.location_lat && body.location_long) {
        body.location = {
          lat: body.location_lat,
          long: body.location_long
        }
        delete body['location_long'];
        delete body['location_lat']
      } else {
        body.location = null;
      }
      this.appService.creteDelivery(body).subscribe((data: any) => {
        this.notificationService.success('Delivery created', `Delivery successfully created, <b>Delivery ID ${data.delivery_id}</b>`, { nzPlacement: 'bottomRight' })
        this.deliveryForm.reset()
      })

    } else {
      Object.values(this.deliveryForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

}
