import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  listOfPackages: any = [];
  listOfDeliveries: any = [];
  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.appService.getAllPackages().subscribe(packages => {
      this.listOfPackages = packages;
    });

    this.appService.getAllDeliveries().subscribe(deliveries => {
      this.listOfDeliveries = deliveries;
      console.log(this.listOfDeliveries);
    });
  }

}

