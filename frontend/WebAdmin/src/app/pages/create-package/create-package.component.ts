
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-create-package',
  templateUrl: './create-package.component.html',
  styleUrls: ['./create-package.component.scss']
})
export class CreatePackageComponent implements OnInit {
  public packageForm!: FormGroup;

  constructor(private fb: FormBuilder, private appService: AppService, private notificationService: NzNotificationService) { }

  ngOnInit(): void {
    this.packageForm = this.fb.group({
      description: [null, [Validators.required, Validators.minLength(3)]],
      weight: [0, [Validators.required]],
      width: [0, [Validators.required, Validators.min(0)]],
      height: [0, [Validators.required, Validators.min(0)]],
      depth: [0, [Validators.required, Validators.min(0)]],
      from_name: [null, [Validators.required, Validators.minLength(2)]],
      from_address: [null, [Validators.required]],
      from_location_lat: [null, [Validators.required]],
      from_location_long: [null, [Validators.required]],
      to_name: [null, [Validators.required]],
      to_address: [null, [Validators.required]],
      to_location_lat: [null, [Validators.required]],
      to_location_long: [null, [Validators.required]],
    })
  }


  submitForm() {
    if (this.packageForm.valid) {
      console.log(this.packageForm.value)

      let body = this.packageForm.value

      body.from_location = {
        lat: body.from_location_lat,
        long: body.from_location_long
      }

      body.to_location = {
        lat: body.to_location_lat,
        long: body.to_location_long
      }
      delete body['to_location_lat']
      delete body['to_location_long']
      delete body['from_location_lat']
      delete body['from_location_long']

      this.appService.cretePackage(body)
        .subscribe(
          (pkg: any) => {
            this.notificationService.success('Package created', `Package successfully created, <b>Package ID ${pkg.package_id}</b>`, {nzPlacement: 'bottomRight'})
            this.packageForm.reset()
          }
        )
    } else {
      Object.values(this.packageForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

}
