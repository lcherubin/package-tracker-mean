import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { CreateDeliveryComponent } from './create-delivery/create-delivery.component';
import { CreatePackageComponent } from './create-package/create-package.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'create-package', component: CreatePackageComponent },
      { path: 'create-delivery', component: CreateDeliveryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
