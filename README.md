# Package Tracker

## Description

**Package tracking is a web application suite consisting of the following:**

- [x] ***Web Tracker*** - customer client web application (SPA)
- [x] ***Web Driver*** - driver client web application (SPA)
- [x] ***Web Admin*** - admin back-office client web application (SPA)
- [x] ***REST API*** - business process back-end services
- [x] ***Websocket Events*** - for real time updates and communication

## Tech stack

***MEAN:***

- MongoDB
- Express
- Angular
- Node.js

## Running the App

### Development server

***Requirements***

This application is developed with the following versions of tools:

- **Node.js:  v16.3.0**
- **npm: 8.3.0**
- **Angular CLI: 12.2.8**
- **MongoDB:  v4.4.1**
  
Install the tools and follow these steps to run:

#### REST API & Websocket Events

***Install dependencies***

Open your terminal at *backend/api* and type: `npm i`

Create env file : `cp .env.exemple .env`  and update config with the appropriate values

***Running  tests***

Open your terminal at *backend/api* and type: `npm test`

***Runing the server***

Open your terminal at *backend/api* and type: `npm start`
Url:  

- [REST API: http://localhost:9090/docs](http://localhost:9090/docs)
- [Websocket Events:](ws://localhost:9090) `ws://localhost:9090/websocket/delivery/{delivery_id}`

#### Web Admin

***Install dependencies***

Open your terminal at *frontend/WebAdmin* and type: `npm i`

***Running  tests***

Open your terminal at *frontend/WebAdmin* and type: `npm test`

***Runing the server***

Open your terminal at *frontend/WebAdmin* and type: `npm start`

#### Web Driver

***Install dependencies***

Open your terminal at *frontend/WebDriver* and type: `npm i`

***Running  tests***

Open your terminal at *frontend/WebDriver* and type: `npm test`

***Runing the server***

Open your terminal at *frontend/WebDriver* and type: `npm start`

#### Web Tracker

***Install dependencies***

Open your terminal at *frontend/WebTracker* and type: `npm i`

***Running  tests***

Open your terminal at *frontend/WebTracker* and type: `npm test`

***Runing the server***

Open your terminal at *frontend/WebTracker* and type: `npm start`

### Docker

***Requirements***

The app is tested on

- **Docker: 20.10+**
- **docker-compose:  1.29.2**
  
Install  docker & docker-compose and run with `docker-compose up -d`

Default Urls :

- [REST API: http://localhost:9090/docs](http://localhost:9090/docs)
- [Web Tracker:  http://localhost:90](http://localhost:90)
- [Web Driver:  http://localhost:88](http://localhost:88)
- [Web Admin:  http://localhost:85](http://localhost:85)
- [Websocket Events:](ws://localhost:9090) `ws://localhost:9090/websocket/delivery/{delivery_id}`

### Possible improvements

- [ ] Update web apps to make them the responsive
- [ ] Add e2e tests for web apps
- [ ] Add authentication on the backend
- [ ] Add Spinner for long running requests
