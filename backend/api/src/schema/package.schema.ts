import { object, number, string, TypeOf } from "zod";

/**
 * @openapi
 * components:
 *   schema:
 *     Package:
 *       type: object
 *       required:
 *         - description
 *         - weight
 *         - width
 *         - width
 *         - height
 *         - depth
 *         - from_location
 *         - from_name
 *         - from_address
 *         - to_location
 *         - to_name
 *         - to_address
 *       properties:
 *         package_id:
 *           type: string
 *         active_delivery_id:
 *           type: string
 *         description:
 *           type: string
 *         weight:
 *            type: number
 *         width:
 *            type: mumber
 *         height:
 *            type: number
 *         depth:
 *            type: number
 *         from_name:
 *            type: string
 *         from_address:
 *            type: string
 *         from_location: 
 *            type: object
 *            required:
 *              - lat
 *              - long
 *            properties:
 *              lat:
 *                type: number
 *              long:
 *                type: number
 *         to_name:
 *            type: string
 *         to_address:
 *            type: string
 *         to_location: 
 *            type: object
 *            required:
 *              - lat
 *              - long
 *            properties:
 *              lat:
 *                type: number
 *              long:
 *                type: number
 *        
 */

const payload = {
  body: object({
    description: string({
      required_error: "Description  is required",
    }),

    weight: number({
      required_error: "Width  is required",
    }).nonnegative(),

    width: number({
      required_error: "Width  is required",
    }).nonnegative(),

    height: number({
      required_error: "Height  is required",
    }).nonnegative(),
    depth: number({
      required_error: "depth  is required",
    }).nonnegative(),

    from_name: string({
      required_error: "from_name  is required",
    }),

    from_address: string({
      required_error: "from_address  is required",
    }),

    from_location: object({
      lat: number({
        required_error: "latitude (lat)  is required",
      }), long: number({
        required_error: "longitude (long)  is required",
      })
    }),

    to_name: string({
      required_error: "to_name  is required",
    }),

    to_address: string({
      required_error: "to_address  is required",
    }),

    to_location: object({
      lat: number({
        required_error: "latitude (lat)  is required",
      }), long: number({
        required_error: "longitude (long)  is required",
      })
    })
  }),
};

const params = {
  params: object({
    id: string({
      required_error: "id is required",
    }),
  }),
};

export const createPackageSchema = object({
  ...payload,
});

export const updatePackageSchema = object({
  ...payload,
  ...params,
});

export const deletePackageSchema = object({
  ...params,
});

export const getPackageSchema = object({
  ...params,
});

export type CreatePackageInput = TypeOf<typeof createPackageSchema>;
export type UpdatePackageInput = TypeOf<typeof updatePackageSchema>;
export type ReadPackageInput = TypeOf<typeof getPackageSchema>;
export type DeletePackageInput = TypeOf<typeof deletePackageSchema>;
