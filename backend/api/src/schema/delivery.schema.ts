import { Status } from './../models/status';
import { object, number, string, TypeOf, date, nativeEnum, ZodNativeEnum } from "zod";


/**
 * @openapi
 * components:
 *   schema:
 *     Delivery:
 *       type: object
 *       required:
 *         - package_id
 *         - status
 *       properties:
 *         package_id:
 *           type: string
 *         status:
 *            type: string
 *         pickup_time:
 *            type: string
 *            format: date-time
 *         start_time:
 *            type: string
 *            format: date-time
 *         end_time:
 *            type: string
 *            format: date-time
 *         location:
 *            type: object
 *            required:
 *              - lat
 *              - long
 *            properties:
 *              lat:
 *                type: number
 *              long:
 *                type: number 
 *        
 */
const payload = {
  body: object({
    package_id: string({
      required_error: "Package Id is required",
    }),
    pickup_time: string({
      required_error: "pickup date is required",
    }).nullable(),
    start_time: string({
    }).nullable(),
    end_time: string({
    }).nullable(),
    location: object({
      lat: number({
        required_error: "latitude (lat)  is required",
      }), long: number({
        required_error: "longitude (long)  is required",
      })
    }).nullable(),
    status: nativeEnum(Status).nullable()
  }),

};

const params = {
  params: object({
    id: string({
      required_error: "id is required",
    }),
  }),
};

export const createDeliverySchema = object({
  ...payload,
});

export const updateDeliverySchema = object({
  ...payload,
  ...params,
});

export const deleteDeliverySchema = object({
  ...params,
});

export const getDeliverySchema = object({
  ...params,
});

export type CreateDeliveryInput = TypeOf<typeof createDeliverySchema>;
export type UpdateDeliveryInput = TypeOf<typeof updateDeliverySchema>;
export type ReadDeliveryInput = TypeOf<typeof getDeliverySchema>;
export type DeleteDeliveryInput = TypeOf<typeof deleteDeliverySchema>;
