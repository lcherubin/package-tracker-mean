

import { DeliveryDocument } from './delivery.model';
import { EventType } from './event-type';
import { Point } from './point';
import { Status } from './status';


export interface MessagePayload {
    event: EventType,
    delivery_id: string,
    location?: Point,
    status?: Status,
    // delivery_object?: DeliveryDocument,
}

