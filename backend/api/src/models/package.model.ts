
import mongoose from 'mongoose';
import { Point } from './point';

/**
 * Package Input model class
 *
 * @export
 * @interface PackageInput
 */
export interface PackageInput {
    package_id?: string
    active_delivery_id?: string,
    description?: string,
    weight?: number,
    width?: number,
    height?: number,
    depth?: number,
    from_name?: string,
    from_address?: string,
    from_location?: Point
    to_name?: string,
    to_address?: string,
    to_location?: Point,
}

/**
 *pacakage document
 *
 * @export
 * @interface PackageDocument
 * @extends {PackageInput}
 * @extends {mongoose.Document}
 */
export interface PackageDocument extends PackageInput, mongoose.Document {
    createdAt: Date
    updatedAt: Date
}

/** @type {*}  mongo schema for package*/
const packageSchema = new mongoose.Schema({
    package_id: { type: String, unique: true },
    active_delivery_id: { type: String },
    description: { type: String },
    weight: { type: Number },
    width: { type: Number },
    height: { type: Number },
    depth: { type: Number },
    from_name: { type: String },
    from_address: { type: String },
    from_location: {
        lat:  Number ,
        long:  Number 
    },
    to_name: { type: String },
    to_address: { type: String },
    to_location:{
        lat:  Number ,
        long:  Number 
    },
}, {
    timestamps: true
})

const PackageModel = mongoose.model('Package', packageSchema);

export default PackageModel;