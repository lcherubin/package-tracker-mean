export enum Status {
    Open = "open",
    PickedUp = "picked-up",
    InTransit = "in-transit",
    Delivered = "delivered",
    Failed = "failed"
}