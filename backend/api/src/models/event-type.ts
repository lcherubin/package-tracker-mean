export enum EventType {
    LocationChanged = 'location_changed',
    StatusChanged = 'status_changed',
    DeliveryUpdated = 'delivery_updated'
}
