import mongoose from 'mongoose';
import { Point } from './point';
import { Status } from './status';

export interface DeliveryInput {
    delivery_id?: string;
    package_id?: string;
    pickup_time?: Date ;
    start_time?: Date ;
    end_time?: Date ;
    location?: Point;
    status?: Status ;
}

// export enum Status {
//     Open = "open",
//     PickedUp = "picked-up",
//     InTransit = "in-transit",
//     Delivered = "delivered",
//     Failed = "failed"
// }

export interface DeliveryDocument extends DeliveryInput, mongoose.Document {
    createdAt: Date
    updatedAt: Date
}



const packageSchema = new mongoose.Schema({
    delivery_id: { type: String, unique: true, },
    package_id: { type: String },
    pickup_time: { type: Date },
    start_time: { type: Date },
    end_time: { type: Date },
    location: {
        lat: Number,
        long: Number
    },
    status: { type: String, }
}, {
    timestamps: true
})



const DeliveryModel = mongoose.model('Delivery', packageSchema);

export default DeliveryModel;