import  logger  from './utils/logger';
import { EventType } from "./models/event-type";
import { MessagePayload } from "./models/message-payload";
import { handleDeliveryWsEvent } from "./services/delivery.service";




function wsRoutes(app: any, expressWs: any) {

    app.ws('/websocket/delivery/:id', function (ws: any, req: any) {
        ws.on('message', async function (msg: any) {

            // parse the incomming message
            const payload = JSON.parse(msg) as MessagePayload

            // handle the message event
            let delivery = await handleDeliveryWsEvent(payload)

            if (delivery) {
                // broadcast delivery updates message
                const data: string = JSON.stringify({ event: EventType.DeliveryUpdated, delivery_object: delivery })
                expressWs().clients.forEach(function (client: any) {
                    client.send(data)
                });
            }

        });
        // ws.send(JSON.stringify({ "message": "Hello from Tracker app" }))
    });

}

export default wsRoutes