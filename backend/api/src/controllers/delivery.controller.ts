import  logger  from '../utils/logger';
import { Request, Response } from "express";
import { CreateDeliveryInput, UpdateDeliveryInput } from "../schema/delivery.schema";
import { createDelivery, deleteDelivery, findAllDeliveries, findAndUpdateDelivery, findDelivery } from "../services/delivery.service";
import { findAndUpdatePackage, findPackage } from "../services/package.service";

export async function createDeliveryHandler(
    req: Request<{}, {}, CreateDeliveryInput["body"]>, res: Response
) {
    const body = req.body;
    
    const packageId = body.package_id
    const pkg = await findPackage({ package_id: packageId });

    if (!pkg) {
        return res.status(400).send({ 'message': `invalid package_id` });
    }

    // @ts-ignore
    const delivery = await createDelivery({ ...body });

    pkg.active_delivery_id = delivery.delivery_id

    const updpkg = await findAndUpdatePackage({ package_id: packageId }, pkg, {
        new: true,
    });
    logger.debug(updpkg)

    return res.send(delivery);
}

export async function updateDeliveryHandler(
    req: Request<UpdateDeliveryInput["params"]>,
    res: Response
) {

    const deliveryId = req.params.id;
    const update = req.body;

    const delivery = await findDelivery({ delivery_id: deliveryId });

    if (!delivery) {
        return res.sendStatus(404);
    }

    const packageId = update.package_id
   
    const pkg = await findPackage({ package_id: packageId });

    if (!pkg) {
        return res.status(400).send({ 'message': `invalid package_id` });
    }


    const updatedDelivery = await findAndUpdateDelivery({ delivery_id: deliveryId }, update, {
        new: true,
    });

    return res.send(updatedDelivery);
}

export async function getDeliveryHandler(
    req: Request<UpdateDeliveryInput["params"]>,
    res: Response
) {
    const deliveryId = req.params.id;
    const delivery = await findDelivery({ delivery_id: deliveryId });

    if (!delivery) {
        return res.sendStatus(404);
    }

    return res.send(delivery);
}


export async function getAllDeliveriesHandler(
    req: Request<UpdateDeliveryInput["params"]>,
    res: Response
) {

    const deliveries = await findAllDeliveries({});

    return res.send(deliveries);
}

export async function deleteDeliveryHandler(
    req: Request<UpdateDeliveryInput["params"]>,
    res: Response
) {

    const deliveryId = req.params.id;

    const delivery = await findDelivery({ delivery_id: deliveryId });

    if (!delivery) {
        return res.sendStatus(404);
    }

    await deleteDelivery({ delivery_id: deliveryId });

    return res.sendStatus(200);
}


