import { Request, Response } from "express";
import { CreatePackageInput, UpdatePackageInput } from "../schema/package.schema";
import { createPackage, deletePackage, findAllPackages, findAndUpdatePackage, findPackage } from "../services/package.service";

export async function createPackageHandler(
    req: Request<{}, {}, CreatePackageInput["body"]>, res: Response
) {
    const body = req.body;
    const pkg = await createPackage({ ...body });

    return res.send(pkg);
}

export async function updatePackageHandler(
    req: Request<UpdatePackageInput["params"]>,
    res: Response
) {

    const packageId = req.params.id;
    const update = req.body;

    const pkg = await findPackage({ package_id: packageId });

    if (!pkg) {
        return res.sendStatus(404);
    }


    const updatedPackage = await findAndUpdatePackage({ package_id: packageId }, update, {
        new: true,
    });

    return res.send(updatedPackage);
}

export async function getPackageHandler(
    req: Request<UpdatePackageInput["params"]>,
    res: Response
) {
    const packageId = req.params.id;
    const pkg = await findPackage({ package_id: packageId });

    if (!pkg) {
        return res.sendStatus(404);
    }

    return res.send(pkg);
}


export async function getAllPackagesHandler(
    req: Request<UpdatePackageInput["params"]>,
    res: Response
) {

    const pkgs = await findAllPackages({});

    return res.send(pkgs);
}

export async function deletePackageHandler(
    req: Request<UpdatePackageInput["params"]>,
    res: Response
) {

    const packageId = req.params.id;

    const pkg = await findPackage({ package_id: packageId });

    if (!pkg) {
        return res.sendStatus(404);
    }

    await deletePackage({ package_id: packageId });

    return res.sendStatus(200);
}
