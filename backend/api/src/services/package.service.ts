import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import PackageModel, { PackageDocument, PackageInput } from "../models/package.model";
import { Guid } from 'js-guid';
/**
 * Save package to db
 *
 * @export
 * @param {PackageInput} input the package to save
 * @return {PackageModel} the newlly created package
 */
export async function createPackage(input: PackageInput) {

    try {
        input.package_id = Guid.newGuid().toString();
        return PackageModel.create(input);
    } catch (e) {
        throw e;
    }
}

/**
 * Find one package
 *
 * @export
 * @param {FilterQuery<PackageDocument>} query
 * @param {QueryOptions} [options={ lean: true }]
 * @return {*} 
 */
export async function findPackage(
    query: FilterQuery<PackageDocument>,
    options: QueryOptions = { lean: true }
) {

    try {
        return PackageModel.findOne(query, {}, options);
    } catch (e) {

        throw e;
    }
}

/**
 * Find all packages in the database
 *
 * @export
 * @param {FilterQuery<PackageDocument>} query
 * @param {QueryOptions} [options={ lean: true }]
 * @return {array<Package>} the packages in the database
 */
export async function findAllPackages(
    query: FilterQuery<PackageDocument>,
    options: QueryOptions = { lean: true }
) {

    try {
        return PackageModel.find(query, {}, options);
    } catch (e) {

        throw e;
    }
}

/**
 * find and update the package 
 *
 * @export
 * @param {FilterQuery<PackageDocument>} query
 * @param {UpdateQuery<PackageDocument>} update
 * @param {QueryOptions} options
 * @return {Package}  the updated package document
 */
export async function findAndUpdatePackage(
    query: FilterQuery<PackageDocument>,
    update: UpdateQuery<PackageDocument>,
    options: QueryOptions
) {
    return PackageModel.findOneAndUpdate(query, update, options);
}

/**
 * delete package
 *
 * @export
 * @param {FilterQuery<PackageDocument>} query
 * @return {*} 
 */
export async function deletePackage(query: FilterQuery<PackageDocument>) {
    return PackageModel.deleteOne(query);
}
