import  logger  from '../utils/logger';
import { Guid } from 'js-guid';
import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import DeliveryModel, { DeliveryDocument, DeliveryInput } from "../models/delivery.model";
import { MessagePayload } from '../models/message-payload';
import { EventType } from "../models/event-type";
import { Status } from "../models/status";

/**
 * Save delivery to db
 *
 * @export
 * @param {DeliveryInput} input the delivery to save
 * @return {DeliveryModel} the newlly created delivery
 */
export async function createDelivery(input: DeliveryInput) {

    try {
        input.delivery_id = Guid.newGuid().toString();
        return DeliveryModel.create(input);
    } catch (e) {
        throw e;
    }
}

/**
 * Find one delivery
 *
 * @export
 * @param {FilterQuery<DeliveryDocument>} query
 * @param {QueryOptions} [options={ lean: true }]
 * @return {*} 
 */
export async function findDelivery(
    query: FilterQuery<DeliveryDocument>,
    options: QueryOptions = { lean: true }
) {

    try {
        return DeliveryModel.findOne(query, {}, options);
    } catch (e) {

        throw e;
    }
}

/**
 * Find all deliveries in the database
 *
 * @export
 * @param {FilterQuery<DeliveryDocument>} query
 * @param {QueryOptions} [options={ lean: true }]
 * @return {array<Delivery>} the deliveries in the database
 */
export async function findAllDeliveries(
    query: FilterQuery<DeliveryDocument>,
    options: QueryOptions = { lean: true }
) {

    try {
        return DeliveryModel.find(query, {}, options);
    } catch (e) {

        throw e;
    }
}

/**
 * find and update the delivery 
 *
 * @export
 * @param {FilterQuery<DeliveryDocument>} query
 * @param {UpdateQuery<DeliveryDocument>} update
 * @param {QueryOptions} options
 * @return {Delivery}  the updated delivery document
 */
export async function findAndUpdateDelivery(
    query: FilterQuery<DeliveryDocument>,
    update: UpdateQuery<DeliveryDocument>,
    options: QueryOptions
) {
    return DeliveryModel.findOneAndUpdate(query, update, options);
}

/**
 * delete delivery
 *
 * @export
 * @param {FilterQuery<DeliveryDocument>} query
 * @return {*} 
 */
export async function deleteDelivery(query: FilterQuery<DeliveryDocument>) {
    return DeliveryModel.deleteOne(query);
}


/**
 * Handle incomming websocket events 
 *
 * @export
 * @param {MessagePayload} payload event payload
 * @return {*}  
 */
export async function handleDeliveryWsEvent(payload: MessagePayload) {

    let delivery: any = await findDelivery({ delivery_id: payload.delivery_id });
    if (delivery) {
        switch (payload.event) {
            case EventType.LocationChanged:
                if (payload.location) {
                    delivery.location = payload.location;
                }
                break;
            case EventType.StatusChanged:
                if (payload.status) {

                    delivery.status = payload.status;

                    if(payload.status == Status.PickedUp){
                        delivery.pickup_time = new Date();
                    }

                    if(payload.status == Status.InTransit){
                        delivery.start_time = new Date();
                    }
                   
                    if(payload.status == Status.Delivered){
                        delivery.end_time = new Date();
                    }
                   
                }
                break;
            default:
                return null;
        }

        return findAndUpdateDelivery({ delivery_id: payload.delivery_id }, delivery, { lean: true });
    }
    return null;
}