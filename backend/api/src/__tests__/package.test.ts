import supertest from "supertest";
import * as PackageService from '../services/package.service';
import createServer from "../utils/server";


const app = createServer();

const packagePayload = {
    package_id: "100d5cf1-544d-4d90-9d78-7680240a8526",
    description: "Sac de mais",
    weight: 200,
    width: 80,
    height: 30,
    depth: 40,
    from_name: "Luckmann",
    from_address: "Cotonou",
    from_location: {
        lat: 40,
        long: 90
    },
    to_name: "Lamek Remo",
    to_address: "Calavi",
    to_location: {
        lat: 40,
        long: 90
    },
    _id: "61bb1f8c9ad6ff94888fc41a",
    createdAt: new Date("2021-12-16T11:14:21.008Z"),
    updatedAt: new Date("2021-12-16T11:14:21.008Z"),
    __v: 0
}


const packageInput = {
    description: "Sac de mais",
    weight: 200,
    width: 80,
    height: 30,
    depth: 40,
    from_name: "Luckmann",
    from_address: "Cotonou",
    from_location: {
        lat: 40,
        long: 90
    },
    to_name: "Lamek Remo",
    to_address: "Calavi",
    to_location: {
        lat: 40,
        long: 90
    }
}

const invalidPackageInput = {
    weight: -200,
    width: 80,
    height: 30,
    depth: 40,
    from_name: null,
    from_address: "Cotonou",
    from_location: null,
    to_name: "Lamek Remo",
    to_address: "Calavi",
    to_location: {
        lat: 40,
        long: 90
    }
}




describe("Package api", () => {


    describe("GET /api/package/", () => {
        describe("Given packages exist", () => {
            it("should return 200 status and the list of packages", async () => {

                const findAllPackageServiceMock = jest
                    .spyOn(PackageService, "findAllPackages")
                    // @ts-ignore
                    .mockReturnValueOnce([packagePayload]);
                const { statusCode, body } = await supertest(app).get(`/api/package`);
                expect(statusCode).toBe(200);
                expect(findAllPackageServiceMock).toHaveBeenCalled();
                expect(body.length).not.toEqual(0)
            })
        })

        describe("Given packages doesn't' exist", () => {
            it("should return 200 status and empty list", async () => {

                const findAllPackageServiceMock = jest
                    .spyOn(PackageService, "findAllPackages")
                    // @ts-ignore
                    .mockReturnValueOnce([]);
                const { statusCode, body } = await supertest(app).get(`/api/package`);
                expect(statusCode).toBe(200);
                expect(findAllPackageServiceMock).toHaveBeenCalled();
                expect(body.length).toEqual(0)
            })
        })
    })

    describe("GET /api/package/{id}", () => {
        describe("Given the package doesn't exist", () => {
            it("should return 404", async () => {

                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(null);

                const id = "invalid_package_id";
                const response = await supertest(app).get(`/api/package/${id}`);

                expect(response.statusCode).toBe(404);
                expect(findPackageServiceMock).toHaveBeenCalled();
            })
        })

        describe("Given the package exists", () => {
            it("should return 200 status and the package", async () => {

                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);

                const id = packagePayload.package_id;
                const { statusCode, body } = await supertest(app).get(`/api/package/${id}`);

                expect(statusCode).toBe(200);
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(body.package_id).toEqual(id)
            })
        })
    })


    describe("POST /api/package", () => {
        describe("Given valid package input", () => {
            it("should return 200 status and the package", async () => {
                const createPackageServiceMock = jest
                    .spyOn(PackageService, "createPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);

                const { statusCode, body } = await supertest(createServer())
                    .post("/api/package/")
                    .send(packageInput);

                expect(statusCode).toBe(200);
                expect(createPackageServiceMock).toHaveBeenCalled();
                expect(body).toEqual({
                    package_id: "100d5cf1-544d-4d90-9d78-7680240a8526",
                    description: "Sac de mais",
                    weight: 200,
                    width: 80,
                    height: 30,
                    depth: 40,
                    from_name: "Luckmann",
                    from_address: "Cotonou",
                    from_location: {
                        lat: 40,
                        long: 90
                    },
                    to_name: "Lamek Remo",
                    to_address: "Calavi",
                    to_location: {
                        lat: 40,
                        long: 90
                    },
                    _id: "61bb1f8c9ad6ff94888fc41a",
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String),
                    __v: 0
                })
            })
        })

        describe("Given invalid package input", () => {
            it("should return 400 status", async () => {

                const { statusCode, body } = await supertest(createServer())
                    .post("/api/package/")
                    .send(invalidPackageInput);
                expect(statusCode).toBe(400);

            })
        })
    })

    describe("PUT /api/package/{id}", () => {
        describe("Given the package exist and valid package input", () => {
            it("should return 200 status and the updated package", async () => {

                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);


                const updatePackagePayload = { ...packagePayload };
                updatePackagePayload.__v = 1;
                updatePackagePayload.description = "Climatiseur Neo";

                const updatePackageInput = { ...packagePayload }
                updatePackageInput.description = "Climatiseur Neo";


                const updatePackageServiceMock = jest
                    .spyOn(PackageService, "findAndUpdatePackage")
                    // @ts-ignore
                    .mockReturnValueOnce(updatePackagePayload);

                const { statusCode, body } = await supertest(createServer())
                    .put(`/api/package/${packagePayload.package_id}`)
                    .send(updatePackageInput);

                expect(statusCode).toBe(200);
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(updatePackageServiceMock).toHaveBeenCalled();

                expect(body).toEqual({
                    ...updatePackagePayload,
                    ...{
                        createdAt: expect.any(String),
                        updatedAt: expect.any(String)
                    }
                })
            })
        })

        describe("Given the package id doesn't exist", () => {
            it("should return 404 status", async () => {
                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(null);

                const updatePackagePayload = { ...packagePayload };
                updatePackagePayload.__v = 1;
                updatePackagePayload.description = "Climatiseur Neo";

                const updatePackageInput = { ...packagePayload }

                updatePackageInput.description = "Climatiseur Neo";


                const updatePackageServiceMock = jest
                    .spyOn(PackageService, "findAndUpdatePackage")
                    // @ts-ignore
                    .mockReturnValueOnce(updatePackagePayload);

                const { statusCode } = await supertest(createServer())
                    .put(`/api/package/INAVLID_ID`)
                    .send(updatePackageInput);

                expect(statusCode).toBe(404);
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(updatePackageServiceMock).not.toHaveBeenCalled();
            })
        })



        describe("Given the input package invalid", () => {
            it("should return 400 status", async () => {
                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);

                const updatePackagePayload = { ...packagePayload };
                updatePackagePayload.__v = 1;
                updatePackagePayload.description = "Climatiseur Neo";

                const updatePackageInput = { ...invalidPackageInput }

                const updatePackageServiceMock = jest
                    .spyOn(PackageService, "findAndUpdatePackage")
                    // @ts-ignore
                    .mockReturnValueOnce(updatePackagePayload);

                const { statusCode } = await supertest(createServer())
                    .put(`/api/package/${packagePayload.package_id}`)
                    .send(updatePackageInput);

                expect(statusCode).toBe(400);
                expect(findPackageServiceMock).not.toHaveBeenCalled();
                expect(updatePackageServiceMock).not.toHaveBeenCalled();
            })
        })
    })


    describe("DELETE /api/package/{id}", () => {
        describe("Given the package doesn't exist", () => {
            it("should return 404", async () => {

                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(null);
                const deletePackageServiceMock = jest
                    .spyOn(PackageService, "deletePackage")
                    // @ts-ignore
                    .mockReturnValueOnce({});

                const id = "invalid_package_id";
                const response = await supertest(app).delete(`/api/package/${id}`);

                expect(response.statusCode).toBe(404);
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(deletePackageServiceMock).not.toHaveBeenCalled();
            })
        })

        describe("Given the package exists", () => {
            it("should return 200 status", async () => {

                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);

                const deletePackageServiceMock = jest
                    .spyOn(PackageService, "deletePackage")
                    // @ts-ignore
                    .mockReturnValueOnce({});


                const id = packagePayload.package_id;
                const { statusCode } = await supertest(app).delete(`/api/package/${id}`);

                expect(statusCode).toBe(200);
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(deletePackageServiceMock).toHaveBeenCalled();

            })
        })
    })

})