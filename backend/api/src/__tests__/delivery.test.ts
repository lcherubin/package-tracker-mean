import supertest from "supertest";
import * as DeliveryService from '../services/delivery.service';
import * as PackageService from '../services/package.service';
import createServer from "../utils/server";


const app = createServer();


const deliveryPayload = {
    _id: "61bb4848026f56c6b8872577",
    delivery_id: "4347155a-25cf-463b-b915-b99e5247ba45",
    package_id: "68435766-e81f-4ffb-b8c7-b6af6dfde35f",
    pickup_time: "2021-12-16T11:05:56.873Z",
    start_time: "2021-12-16T11:05:56.873Z",
    end_time: null,
    location: {
        lat: 40,
        long: 90
    },
    status: "open",
    createdAt: new Date("2021-12-16T11:14:21.008Z"),
    updatedAt: new Date("2021-12-16T11:14:21.008Z"),
    __v: 0
}

const packagePayload = {
    package_id: "68435766-e81f-4ffb-b8c7-b6af6dfde35f",
    description: "Sac de mais",
    weight: 200,
    width: 80,
    height: 30,
    depth: 40,
    from_name: "Luckmann",
    from_address: "Cotonou",
    from_location: {
        lat: 40,
        long: 90
    },
    to_name: "Lamek Remo",
    to_address: "Calavi",
    to_location: {
        lat: 40,
        long: 90
    },
    _id: "61bb1f8c9ad6ff94888fc41a",
    createdAt: new Date("2021-12-16T11:14:21.008Z"),
    updatedAt: new Date("2021-12-16T11:14:21.008Z"),
    __v: 0
}


const deliveryInput = {
    package_id: "68435766-e81fb-b8c7-b6af6dfde35f",
    pickup_time: "2021-12-16 11:05:56",
    start_time: "2021-12-16 11:05:56",
    end_time: null,
    location: {
        lat: 40,
        long: 90
    },
    status: "open"
}

const invalidDeliveryInput = {
    package_id: "68435766-e81fb-b8c7-b6af6dfde35f",
    pickup_time: "2021-12-16 11:05:56",
    start_time: "2021-12-16 11:05:56",
    end_time: null,
    location: {
        lat: 40,
        long: 90
    },
    status: "invalid status"
}




describe("Delivery api", () => {

    describe("GET /api/delivery/", () => {
        describe("Given deliveries exist", () => {
            it("should return 200 status and the list of deliveries", async () => {

                const findAllDeliveriesServiceMock = jest
                    .spyOn(DeliveryService, "findAllDeliveries")
                    // @ts-ignore
                    .mockReturnValueOnce([deliveryPayload]);
                const { statusCode, body } = await supertest(app).get(`/api/delivery`);
                expect(statusCode).toBe(200);
                expect(findAllDeliveriesServiceMock).toHaveBeenCalled();
                expect(body.length).not.toEqual(0)
            })
        })

        describe("Given deliveries doesn't' exist", () => {
            it("should return 200 status and empty list", async () => {

                const findAllDeliveriesServiceMock = jest
                    .spyOn(DeliveryService, "findAllDeliveries")
                    // @ts-ignore
                    .mockReturnValueOnce([]);
                const { statusCode, body } = await supertest(app).get(`/api/delivery`);
                expect(statusCode).toBe(200);
                expect(findAllDeliveriesServiceMock).toHaveBeenCalled();
                expect(body.length).toEqual(0)
            })
        })
    })

    describe("GET /api/delivery/{id}", () => {
        describe("Given the delivery doesn't exist", () => {
            it("should return 404", async () => {

                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(null);

                const id = "invalid_delivery_id";
                const response = await supertest(app).get(`/api/delivery/${id}`);

                expect(response.statusCode).toBe(404);
                expect(findDeliveryServiceMock).toHaveBeenCalled();
            })
        })

        describe("Given the delivery exists", () => {
            it("should return 200 status and the delivery", async () => {

                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(deliveryPayload);

                const id = deliveryPayload.delivery_id;
                const { statusCode, body } = await supertest(app).get(`/api/delivery/${id}`);

                expect(statusCode).toBe(200);
                expect(findDeliveryServiceMock).toHaveBeenCalled();
                expect(body.delivery_id).toEqual(id)
            })
        })
    })


    describe("POST /api/delivery", () => {
        describe("Given valid delivery input", () => {
            it("should return 200 status and the delivery", async () => {

                const createDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "createDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(deliveryPayload);

                const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);


                const updatedPkg = { ...packagePayload, active_delivery_id: deliveryPayload.delivery_id }
                const updatePackageServiceMock = jest
                    .spyOn(PackageService, "findAndUpdatePackage")
                    // @ts-ignore
                    .mockReturnValueOnce(updatedPkg);

                const { statusCode, body } = await supertest(createServer())
                    .post("/api/delivery/")
                    .send(deliveryInput);

                expect(createDeliveryServiceMock).toHaveBeenCalled();
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(updatePackageServiceMock).toHaveBeenCalled();
                expect(statusCode).toBe(200);


                expect(body).toEqual({
                    _id: "61bb4848026f56c6b8872577",
                    delivery_id: "4347155a-25cf-463b-b915-b99e5247ba45",
                    package_id: "68435766-e81f-4ffb-b8c7-b6af6dfde35f",
                    pickup_time: "2021-12-16T11:05:56.873Z",
                    start_time: "2021-12-16T11:05:56.873Z",
                    end_time: null,
                    location: {
                        lat: 40,
                        long: 90
                    },
                    status: "open",
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String),
                    __v: 0
                })
            })
        })

        describe("Given invalid delivery input", () => {
            it("should return 400 status", async () => {
                const createDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "createDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(deliveryPayload);
                const { statusCode, body } = await supertest(createServer())
                    .post("/api/delivery/")
                    .send(invalidDeliveryInput);
                expect(statusCode).toBe(400);
                expect(createDeliveryServiceMock).not.toHaveBeenCalled();
            })
        })
    })

    describe("PUT /api/delivery/{id}", () => {
        describe("Given the delivery exist and valid delivery input", () => {
            it("should return 200 status and the updated delivery", async () => {

                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(deliveryPayload);

                    const findPackageServiceMock = jest
                    .spyOn(PackageService, "findPackage")
                    // @ts-ignore
                    .mockReturnValueOnce(packagePayload);

                const updateDeliveryPayload = { ...deliveryPayload };
                updateDeliveryPayload.__v = 1;
                updateDeliveryPayload.status = "in-transit";

                const updateDeliveryInput = { ...deliveryPayload }
                updateDeliveryInput.status = "in-transit";


                const updateDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findAndUpdateDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(updateDeliveryPayload);

                const { statusCode, body } = await supertest(createServer())
                    .put(`/api/delivery/${deliveryPayload.delivery_id}`)
                    .send(updateDeliveryInput);

                expect(statusCode).toBe(200);
                expect(findDeliveryServiceMock).toHaveBeenCalled();
                expect(findPackageServiceMock).toHaveBeenCalled();
                expect(updateDeliveryServiceMock).toHaveBeenCalled();

                expect(body).toEqual({
                    ...updateDeliveryPayload,
                    ...{
                        createdAt: expect.any(String),
                        updatedAt: expect.any(String)
                    }
                })
            })
        })

        describe("Given the delivery id doesn't exist", () => {
            it("should return 404 status", async () => {
                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(null);

                const updateDeliveryPayload = { ...deliveryPayload };
                updateDeliveryPayload.__v = 1;
                updateDeliveryPayload.status = "in-transit";

                const updateDeliveryInput = { ...deliveryPayload }

                updateDeliveryInput.status = "in-transit";


                const updateDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findAndUpdateDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(updateDeliveryPayload);

                const { statusCode } = await supertest(createServer())
                    .put(`/api/delivery/INAVLID_ID`)
                    .send(updateDeliveryInput);

                expect(statusCode).toBe(404);
                expect(findDeliveryServiceMock).toHaveBeenCalled();
                expect(updateDeliveryServiceMock).not.toHaveBeenCalled();
            })
        })



        describe("Given the input delivery invalid", () => {
            it("should return 400 status", async () => {
                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(deliveryPayload);

                const updateDeliveryPayload = { ...deliveryPayload };
                updateDeliveryPayload.__v = 1;
                updateDeliveryPayload.status = "in-transit";

                const updateDeliveryInput = { ...invalidDeliveryInput }

                const updateDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findAndUpdateDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(updateDeliveryPayload);

                const { statusCode } = await supertest(createServer())
                    .put(`/api/delivery/${deliveryPayload.delivery_id}`)
                    .send(updateDeliveryInput);

                expect(statusCode).toBe(400);
                expect(findDeliveryServiceMock).not.toHaveBeenCalled();
                expect(updateDeliveryServiceMock).not.toHaveBeenCalled();
            })
        })
    })


    describe("DELETE /api/delivery/{id}", () => {
        describe("Given the delivery doesn't exist", () => {
            it("should return 404", async () => {

                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(null);
                const deleteDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "deleteDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce({});

                const id = "invalid_delivery_id";
                const response = await supertest(app).delete(`/api/delivery/${id}`);

                expect(response.statusCode).toBe(404);
                expect(findDeliveryServiceMock).toHaveBeenCalled();
                expect(deleteDeliveryServiceMock).not.toHaveBeenCalled();
            })
        })

        describe("Given the delivery exists", () => {
            it("should return 200 status", async () => {

                const findDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "findDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce(deliveryPayload);

                const deleteDeliveryServiceMock = jest
                    .spyOn(DeliveryService, "deleteDelivery")
                    // @ts-ignore
                    .mockReturnValueOnce({});


                const id = deliveryPayload.delivery_id;
                const { statusCode } = await supertest(app).delete(`/api/delivery/${id}`);

                expect(statusCode).toBe(200);
                expect(findDeliveryServiceMock).toHaveBeenCalled();
                expect(deleteDeliveryServiceMock).toHaveBeenCalled();

            })
        })
    })

})