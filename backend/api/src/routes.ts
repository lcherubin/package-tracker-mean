
import {Request, Response } from 'express'
import { createDeliveryHandler, updateDeliveryHandler, getDeliveryHandler, getAllDeliveriesHandler, deleteDeliveryHandler } from './controllers/delivery.controller';
import { createPackageHandler, deletePackageHandler, getAllPackagesHandler, getPackageHandler, updatePackageHandler } from './controllers/package.controller';
import validate from './middlewares/validate';
import { createDeliverySchema, deleteDeliverySchema, getDeliverySchema, updateDeliverySchema } from './schema/delivery.schema';
import { getPackageSchema, updatePackageSchema, createPackageSchema, deletePackageSchema } from './schema/package.schema';

/**
 * Define the app routes 
 * @param app Exppress app
 */
function routes(app: any) {

    /**
     * @openapi
     * /:
     *  get:
     *     tags:
     *     - AppStatus
     *     description: Responds if the app is up and running
     *     responses:
     *       200:
     *         description: App is up and running
     */
    app.get('/', (req: Request, res: Response) => res.sendStatus(200));

   
    // =============== Package api routes  =======================


    /**
     * @openapi
     * '/api/package':
     *  get:
     *     tags:
     *     - Package
     *     summary: Get all packages
     *     description: Get all packages
     *     responses:
     *       200:
     *         description: Success
     *         content:
     *            application/json:
     *            schema:
     *              type: array
     *              items:
     *                $ref: '#/components/schema/Package'
     */
    app.get("/api/package", getAllPackagesHandler);

    /***
     * @openapi
     * '/api/package':
     *  post:
     *     tags:
     *     - Package
     *     summary: Create new package
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *           schema:
     *              $ref: '#/components/schema/Package'
     *     responses:
     *      200:
     *        description: Success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schema/Package'
     *      400:
     *        description: Bad request
     */
    app.post("/api/package", [validate(createPackageSchema)], createPackageHandler);

    /***
     * @openapi
     * '/api/package/{id}':
     *  put:
     *     tags:
     *     - Package
     *     summary: Update  package
     *     parameters:
     *      - name: id
     *        in: path
     *        description: The id of the package (package_id)
     *        required: true
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *           schema:
     *              $ref: '#/components/schema/Package'
     *     responses:
     *      200:
     *        description: Success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schema/Package'
     *      400:
     *        description: Bad request
     *      404:
     *         description: Not found
     */
    app.put("/api/package/:id", [validate(updatePackageSchema)], updatePackageHandler);
    
    /**
     * @openapi
     * '/api/package/{id}':
     *  get:
     *     tags:
     *     - Package
     *     summary: Get package by id
     *     parameters:
     *      - name: id
     *        in: path
     *        description: The id of the package (package_id)
     *        required: true
     *     responses:
     *       200:
     *         description: Success
     *         content:
     *            application/json:
     *            schema:
     *              type: array
     *              items:
     *                $ref: '#/components/schema/Package'
     *       404:
     *         description: Not found
     */
    app.get("/api/package/:id", validate(getPackageSchema), getPackageHandler);
    
    
    /**
     * @openapi
     * '/api/package/{id}':
     *  delete:
     *     tags:
     *     - Package
     *     summary: Delete package
     *     parameters:
     *      - name: id
     *        in: path
     *        description: The id of the package  (package_id)
     *        required: true
     *     responses:
     *       200:
     *         description: Success
     *       404:
     *         description: Not found
     */              
    app.delete("/api/package/:id", [validate(deletePackageSchema)], deletePackageHandler);



    // =============== Delivery api routes  =======================

    /**
     * @openapi
     * '/api/delivery':
     *  get:
     *     tags:
     *     - Delivery
     *     summary: Get all deliveries
     *     description: Get all deliveries
     *     responses:
     *       200:
     *         description: Success
     *         content:
     *            application/json:
     *            schema:
     *              type: array
     *              items:
     *                $ref: '#/components/schema/Delivery'
     */    
    app.get("/api/delivery", getAllDeliveriesHandler);

    /***
     * @openapi
     * '/api/delivery':
     *  post:
     *     tags:
     *     - Delivery
     *     summary: Create new delivery
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *           schema:
     *              $ref: '#/components/schema/Delivery'
     *     responses:
     *      200:
     *        description: Success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schema/Delivery'
     *      400:
     *        description: Bad request
     */    
    app.post("/api/delivery", [validate(createDeliverySchema)], createDeliveryHandler);
    
    /***
     * @openapi
     * '/api/delivery/{id}':
     *  put:
     *     tags:
     *     - Delivery
     *     summary: Update delivery
     *     parameters:
     *      - name: id
     *        in: path
     *        description: The id of the delivery (delivery_id)
     *        required: true
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *           schema:
     *              $ref: '#/components/schema/Delivery'
     *     responses:
     *      200:
     *        description: Success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schema/Delivery'
     *      400:
     *        description: Bad request
     *      404:
     *         description: Not found
     */    
    app.put("/api/delivery/:id", [validate(updateDeliverySchema)], updateDeliveryHandler);
    
    /**
     * @openapi
     * '/api/delivery/{id}':
     *  get:
     *     tags:
     *     - Delivery
     *     summary: Get delivery by id
     *     parameters:
     *      - name: id
     *        in: path
     *        description: The id of the delivery (delivery_id)
     *        required: true
     *     responses:
     *       200:
     *         description: Success
     *       404:
     *         description: Not found
     */    
    app.get("/api/delivery/:id", validate(getDeliverySchema), getDeliveryHandler);
    
    
    /**
     * @openapi
     * '/api/delivery/{id}':
     *  delete:
     *     tags:
     *     - Delivery
     *     summary: Delete delivery
     *     parameters:
     *      - name: id
     *        in: path
     *        description: The id of the delivery (delivery_id)
     *        required: true
     *     responses:
     *       200:
     *         description: Success
     */
    app.delete("/api/delivery/:id", [validate(deleteDeliverySchema)], deleteDeliveryHandler);



}


export default routes;