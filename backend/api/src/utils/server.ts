import express from "express";
import dotenv from "dotenv";
dotenv.config();
import config from 'config';

import routes from "../routes";
import expressWs from 'express-ws';
import wsRoutes from "../websocket.routes";
import swaggerDocs from "./swagger";

const port = config.get<number>('port');

function createServer() {
    
    const { app, getWss, applyTo } = expressWs(express());

  
    app.use(express.json());
  
  
    routes(app);
  
    wsRoutes(app, getWss);

    swaggerDocs(app, port);

    return app;
  }


  export default createServer